var icons = document.querySelectorAll('.icon');
var previousRectLeft = 0;

icons.forEach(function (icon) {
    icon.onclick = function(e){
        if(e.srcElement.tagName.toLowerCase() === 'i') {
            animateTo(e.srcElement.parentNode);
            return;
        }
        animateTo(e.srcElement);

    }
});


animateTo = function(element) {
    var leftCover = document.querySelector('.left__cover');
    var rightCover = document.querySelector('.right__cover');
    var elementBounds = element.getBoundingClientRect();
    var leftDelay = 0, rightDelay = .1;

    console.log(previousRectLeft, elementBounds);

    if(elementBounds.left > previousRectLeft){
        leftDelay = .1;
        rightDelay = 0;
    }
    previousRectLeft = elementBounds.left;

    var left = TweenMax.to(leftCover,.5,{x:elementBounds.left-7, ease: Power2.easeInOut, delay:leftDelay});
    var right = TweenMax.to(rightCover,.5,{x:elementBounds.left-7, ease: Power2.easeInOut, delay:rightDelay});
};
